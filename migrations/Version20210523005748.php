<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523005748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE company_category (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_category_company (company_category_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_A69CB58AB97257A (company_category_id), INDEX IDX_A69CB58A979B1AD6 (company_id), PRIMARY KEY(company_category_id, company_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_category_category (company_category_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_EDAB0C42B97257A (company_category_id), INDEX IDX_EDAB0C4212469DE2 (category_id), PRIMARY KEY(company_category_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_category_company ADD CONSTRAINT FK_A69CB58AB97257A FOREIGN KEY (company_category_id) REFERENCES company_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_category_company ADD CONSTRAINT FK_A69CB58A979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_category_category ADD CONSTRAINT FK_EDAB0C42B97257A FOREIGN KEY (company_category_id) REFERENCES company_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company_category_category ADD CONSTRAINT FK_EDAB0C4212469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company DROP category');
        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL, DROP name, CHANGE email email VARCHAR(180) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE company_category_company DROP FOREIGN KEY FK_A69CB58AB97257A');
        $this->addSql('ALTER TABLE company_category_category DROP FOREIGN KEY FK_EDAB0C42B97257A');
        $this->addSql('DROP TABLE company_category');
        $this->addSql('DROP TABLE company_category_company');
        $this->addSql('DROP TABLE company_category_category');
        $this->addSql('ALTER TABLE company ADD category INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74 ON user');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP roles, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
