<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Company;
use App\Form\CompanyType;
use App\Form\FinderType;
use Doctrine\Migrations\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompaniesController extends AbstractController
{
    /**
     * @Route("/companies", name="companies")
     */
    public function index(Request $request): Response
    {
        $session_user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        // Search query
        $form = $this->createForm(FinderType::class, null, [
            'action' => $this->generateUrl('index'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && isset($form->getData()['q'])) {
            $q = $form->getData()['q'];
            $companies = $this->getDoctrine()->getRepository(Company::class)->findAllBySearch($q);
        } else {
            $companies = $entityManager->getRepository(Company::class)->findAll();
        }

        return $this->render('companies/index.html.twig', [
            'companies' => $companies,
            'session_user' => $session_user,
        ]);
    }

    /**
     * @Route("/companies/add", name="add")
     */
    public function add(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company, [
            'action' => $this->generateUrl('company_add'),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($company);
            $entityManager->flush();

            $this->addFlash('success', 'Company successfully added!');
            return $this->redirect($this->generateUrl('company'));
        }
        $categories = $entityManager->getRepository(Category::class)->findAll();

        return $this->render('companies/add.html.twig', [
            'company_form' => $form->createView(),
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/companies/edit/{id}", name="edit")
     */
    public function edit(int $id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($id) {
            $company = $entityManager->getRepository(Company::class)->findOneBy((['id' => $id]));
        }

        $form = $this->createForm(CompanyType::class, $company, [
            'action' => $this->generateUrl('company_edit', ['id' => $id]),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //save on DB
            $entityManager->persist($company);
            $entityManager->flush();
            
            $this->addFlash('success', 'Company successfully edited!');
            return $this->redirect($this->generateUrl('company'));
        }

        $categories = $entityManager->getRepository(Category::class)->findAll();

        return $this->render('companies/add.html.twig', [
            'company_form' => $form->createView(),
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/companies/view/{id}", name="add")
     */
    public function view(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $company = $entityManager->getRepository(Company::class)->findOneBy((['id' => $id]));
        if (!$company) {
            $this->addFlash('error', 'Error accessing Company');
            $this->redirect($this->generateUrl('/'));
        }
        return $this->render('companies/view.html.twig', [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/companies/delete", name="delete")
     */
    public function delete(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $company = $entityManager->getRepository(Company::class)->findOneBy((['id' => $id]));

        if ($company) {
            $entityManager->remove($company);
            $entityManager->flush();
        }

        return $this->render('companies/index.html.twig');
    }
}
