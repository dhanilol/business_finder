<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\FinderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FinderController extends AbstractController
{
    /**
     * @Route("/", name="finder")
     */
    public function index(): Response
    {
        $form = $this->createForm(FinderType::class, null, [
            'action' => $this->generateUrl('company')
        ]);

        return $this->render('finder/index.html.twig', [
            'finder_form' => $form->createView(),
        ]);
    }
}
