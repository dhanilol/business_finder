<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AppController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // if (!$this->getUser()) {
        //     return $this->redirect($this->generateUrl('auth'));        
        // }

        return $this->render('admin/dashboard.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
}
