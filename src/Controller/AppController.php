<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/app", name="app")
     */
    public function index(): Response
    {
        // if (!$isAuthenticated) {
        //     $response = $this->forward('App\Controller\OtherController::fancy', [
        //         'name'  => $name,
        //         'color' => 'green',
        //     ]);
        //    return $response;
        // }
    
        // ... further modify the response or return it directly
    

        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }
}
