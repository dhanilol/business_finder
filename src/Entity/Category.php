<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=CompanyCategory::class, mappedBy="category_id")
     */
    private $companyCategories;

    public function __construct()
    {
        $this->companyCategories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|CompanyCategory[]
     */
    public function getCompanyCategories(): Collection
    {
        return $this->companyCategories;
    }

    public function addCompanyCategory(CompanyCategory $companyCategory): self
    {
        if (!$this->companyCategories->contains($companyCategory)) {
            $this->companyCategories[] = $companyCategory;
            $companyCategory->addCategoryId($this);
        }

        return $this;
    }

    public function removeCompanyCategory(CompanyCategory $companyCategory): self
    {
        if ($this->companyCategories->removeElement($companyCategory)) {
            $companyCategory->removeCategoryId($this);
        }

        return $this;
    }
}
