<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('phone', Type\NumberType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('address', Type\TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('zip', Type\NumberType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('city', Type\TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('state', Type\TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', Type\TextareaType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            // ->add('category', Type\ChoiceType::class, [
            //     'choices' => [
                    
            //     ],
            //     'attr' => [
            //         'class' => 'form-control'
            //     ]
            // ])
            // ->add('Category')
            // ->add('save', Type\SubmitType::class, [
            //     'attr' => [
            //         'class' => 'btn btn-success'
            //     ]
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
